module.exports = function(CostCenter) {

  CostCenter.validatesUniquenessOf('name', {message: 'Cost Center could be unique'});
  CostCenter.observe('before save', function (ctx, next) {
  if (ctx.instance != undefined && ctx.instance != null) {
            ctx.instance.name=(ctx.instance.name.toLowerCase());
            ctx.instance.createdTime = new Date();
            next();
          } else {
            if(ctx.data.name!=undefined && ctx.data.name!=null && ctx.data.name!=''){
              ctx.data.name=(ctx.data.name.toLowerCase());
            }
            ctx.data.updatedTime = new Date();
            next();
          }
  });

};
