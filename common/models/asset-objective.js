module.exports = function(Assetobjective) {

  Assetobjective.validatesUniquenessOf('name', {message: 'Name could be unique'});

};
