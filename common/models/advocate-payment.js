var server = require('../../server/server');
module.exports = function(Advocatepayment) {
Advocatepayment.observe('before save', function (ctx, next) {
    if(ctx.data!=undefined && ctx.data!=null){
      ctx.data['date']=new Date();
      next();
    }else {
      ctx.instance.date=new Date();
      next();
    }
  });

 Advocatepayment.observe('loaded', function(ctx, next) {
      if(ctx.instance){
        var Advocate = server.models.Advocate;
        var advocateName=ctx.instance.advocateName;
        ctx.instance['advocateList']=[];
            var advocateId=advocateName;
            Advocate.find({'where':{'advId':advocateId}}, function (err, advocateList) {
                var advocateDetails=advocateList[0];
                ctx.instance.advocateList.push({
                  'name':advocateDetails.name,
                  'email':advocateDetails.email,
                  'advId':advocateDetails.advId
                });
                next();
            });
      }else{
        next();
      }
    });
};
