var server = require('../../server/server');
module.exports = function(Workflow) {
  Workflow.validatesUniquenessOf('workflowId');
  Workflow.validatesUniquenessOf('name');

  Workflow.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.name=(ctx.instance.name.toLowerCase());
      ctx.instance.workflowId=(ctx.instance.workflowId.toLowerCase());
      ctx.instance.createdTime = new Date();
      next();

    } else {
      if(ctx.data.name!=undefined && ctx.data.name!=null && ctx.data.name!=''){
        ctx.data.name=(ctx.data.name.toLowerCase());
      }
      if(ctx.data.workflowId!=undefined && ctx.data.workflowId!=null && ctx.data.workflowId!=''){
        ctx.data.workflowId=(ctx.data.workflowId.toLowerCase());
      }
      ctx.data.updatedTime = new Date();
      next();
    }
  });

};
